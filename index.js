// function to get output div

function submitForm() {

	// getting input values

	var inpt_title = document.getElementById("inpt-title").value;
	var inpt_text = document.getElementById("inpt-text").value;

	// to highlight the word

	substr="@ArvindKejriwal";
	inpt_text = inpt_text.replace(substr, '<span class="color">' + substr + '</span>');
	// console.log(inpt_text);


	// if the input is not empty, print the things in div
	if (inpt_title != "" && inpt_text != "") {
		console.log(inpt_text); // for testing
		document.getElementById("otpt-title").innerHTML = inpt_title;
		document.getElementById("otpt-text").innerHTML = inpt_text;

		// to clear the previous input
		document.getElementById("inpt-title").value = "";
		document.getElementById("inpt-text").value = "";
	}
}


// Download image function

function download() {
	const scale = 2;  // quality of image
    const node = document.getElementById("otpt");
	const style = {
		transform: "scale(" + scale + ")",
		transformOrigin: "top left",
		width: node.offsetWidth + "px",
		height: node.offsetHeight + "px",
	};

	const param = {
		height: node.offsetHeight * scale,
		width: node.offsetWidth * scale,
		quality: 1,
		style,
	};

	domtoimage
		.toBlob(node, param)
		.then(function (blob) {
			window.saveAs(blob, "aap-dyk.png");
		});
}

// dark theme

// var currentTheme = 'dark';

// function changeTheme() {
//     document.body.classList.toggle('dark-mode');

//     if (currentTheme === 'dark') {
//         document.getElementById('toggleknop').innerHTML = '<i class="fas fa-sun" id="zon" style="color:#d8c658;"></i>';
//         currentTheme = 'sun';
//     } else {
//         document.getElementById('toggleknop').innerHTML = '<i class="fas fa-moon" id="maan" style="color:black;"></i>';
//         currentTheme = 'dark';
//     }
// }

const currentTheme = localStorage.getItem("theme");
if (currentTheme == "dark") {
    document.getElementById('toggleknop').innerHTML = '<i class="fas fa-sun" id="zon" style="color:#d8c658;"></i>';
  document.body.classList.add("dark-mode");
}

function changeTheme() {
    document.body.classList.toggle("dark-mode");
  
  document.getElementById('toggleknop').innerHTML = '<i class="fas fa-moon" id="maan" style="color:black;"></i>';

  let theme = "light";
  if (document.body.classList.contains("dark-mode")) {
    document.getElementById('toggleknop').innerHTML = '<i class="fas fa-sun" id="zon" style="color:#d8c658;"></i>';
    theme = "dark";
  }
  localStorage.setItem("theme", theme);
}